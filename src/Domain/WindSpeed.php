<?php
declare(strict_types=1);

namespace App\Domain;

final class WindSpeed
{
    public function __construct(private float $distance, private string $units)
    {

    }

    public function asString(): string
    {
        return "{$this->distance} {$this->units}";
    }
}
