<?php
declare(strict_types=1);

namespace App\Domain;

use DateTime;
use DateTimeInterface;

final class WeatherReportDate
{
    public function __construct(private int $timestamp)
    {

    }

    public function asAtomString(): string
    {
        return DateTime::createFromFormat('U', (string)$this->timestamp)
            ->format(DateTimeInterface::ATOM);
    }
}
