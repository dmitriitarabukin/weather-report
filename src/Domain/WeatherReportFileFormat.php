<?php
declare(strict_types=1);

namespace App\Domain;

use Webmozart\Assert\Assert;

final class WeatherReportFileFormat
{
    public const JSON = 'json';
    public const XML = 'xml';

    private string $fileFormat;

    private function __construct(string $fileFormat)
    {
        Assert::inArray(
            $fileFormat,
            [self::JSON, self::XML],
            "Supported weather report file formats: " . self::JSON . '|' . self::XML . ". Got {$fileFormat}"
        );
        $this->fileFormat = $fileFormat;
    }

    public static function fromString(string $fileFormat): self
    {
        return new self($fileFormat);
    }

    public function asString(): string
    {
        return $this->fileFormat;
    }
}
