<?php
declare(strict_types=1);

namespace App\Domain;

use Webmozart\Assert\Assert;

final class Coordinates
{
    private float $longitude;
    private float $latitude;

    private function __construct(float $longitude, float $latitude)
    {
        Assert::range($longitude, -180.00, 180.00, "Longitude is expected to be between -180 and 180. Got {$longitude}");
        $this->longitude = $longitude;
        Assert::range($latitude, -90.00, 90.00, "Latitude is expected to be between -90 and 90. Got {$latitude}");
        $this->latitude = $latitude;
    }

    public static function fromFloat(float $longitude, float $latitude): self
    {
        return new self($longitude, $latitude);
    }

    public function longitude(): float
    {
        return $this->longitude;
    }

    public function latitude(): float
    {
        return $this->latitude;
    }
}
