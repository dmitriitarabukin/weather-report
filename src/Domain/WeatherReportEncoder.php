<?php
declare(strict_types=1);

namespace App\Domain;

use Symfony\Component\Serializer\SerializerInterface;

final class WeatherReportEncoder
{
    public function __construct(
        private SerializerInterface $serializer,
        private WeatherReportViewFactory $weatherReportViewFactory
    )
    {
    }

    public function encodedWeatherReport(WeatherReport $weatherReport, WeatherReportFileFormat $fileFormat): string
    {
        $weatherReportView = $this->weatherReportViewFactory->createFromWeatherReportAndFileFormat($weatherReport, $fileFormat);
        return $this->serializer->serialize($weatherReportView->asArray(), $fileFormat->asString());
    }
}
