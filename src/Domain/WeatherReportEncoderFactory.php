<?php
declare(strict_types=1);

namespace App\Domain;

use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;

final class WeatherReportEncoderFactory
{
    public function create(): WeatherReportEncoder
    {
        return new WeatherReportEncoder(
            new Serializer(encoders: [
                new JsonEncoder(
                    new JsonEncode([JsonEncode::OPTIONS => JSON_UNESCAPED_UNICODE])
                ),
                new XmlEncoder([
                    XmlEncoder::ROOT_NODE_NAME => 'weather-report',
                    XmlEncoder::ENCODER_IGNORED_NODE_TYPES => [XML_PI_NODE]
                ])
            ]),
            new WeatherReportViewFactory()
        );
    }
}
