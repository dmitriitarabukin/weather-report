<?php
declare(strict_types=1);

namespace App\Domain;

final class Humidity
{
    public function __construct(private int $value)
    {

    }

    public function asString(): string
    {
        return (string)$this->value;
    }
}
