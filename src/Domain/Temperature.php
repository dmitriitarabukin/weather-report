<?php
declare(strict_types=1);

namespace App\Domain;

final class Temperature
{
    public function __construct(private float $degrees, private string $units)
    {

    }

    public function asString(): string
    {
        return "{$this->degrees} {$this->units}";
    }
}
