<?php
declare(strict_types=1);

namespace App\Domain;

final class WeatherReport
{
    public function __construct(
        private WeatherReportDate $date,
        private Temperature       $temperature,
        private WindDirection     $windDirection,
        private WindSpeed         $windSpeed,
        private Humidity          $humidity,
        private Visibility        $visibility,
        private Coordinates       $coordinates
    )
    {
    }

    public function date(): WeatherReportDate
    {
        return $this->date;
    }

    public function temperature(): Temperature
    {
        return $this->temperature;
    }

    public function windDirection(): WindDirection
    {
        return $this->windDirection;
    }

    public function windSpeed(): WindSpeed
    {
        return $this->windSpeed;
    }

    public function humidity(): Humidity
    {
        return $this->humidity;
    }

    public function visibility(): Visibility
    {
        return $this->visibility;
    }

    public function coordinates(): Coordinates
    {
        return $this->coordinates;
    }
}
