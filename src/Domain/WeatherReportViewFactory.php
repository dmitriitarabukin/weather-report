<?php
declare(strict_types=1);

namespace App\Domain;

use App\Application\WeatherReportForJsonFile;
use App\Application\WeatherReportForXmlFile;
use App\Application\WeatherReportView;
use RuntimeException;

final class WeatherReportViewFactory
{
    public function createFromWeatherReportAndFileFormat(WeatherReport $weatherReport, WeatherReportFileFormat $fileFormat): WeatherReportView
    {
        return match ($fileFormat->asString()) {
            WeatherReportFileFormat::JSON => WeatherReportForJsonFile::fromWeatherReport($weatherReport),
            WeatherReportFileFormat::XML => WeatherReportForXmlFile::fromWeatherReport($weatherReport),
            default => throw new RuntimeException('Unsupported weather report file format'),
        };
    }
}
