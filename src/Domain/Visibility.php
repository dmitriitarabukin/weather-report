<?php
declare(strict_types=1);

namespace App\Domain;

final class Visibility
{
    public function __construct(private int $distance, private string $units)
    {

    }

    public function asString(): string
    {
        return "{$this->distance} {$this->units}";
    }
}
