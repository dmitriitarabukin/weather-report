<?php
declare(strict_types=1);

namespace App\Domain;

final class WindDirection
{
    public function __construct(private int $amount, private string $units)
    {

    }

    public function asString(): string
    {
        return "{$this->amount} {$this->units}";
    }
}
