<?php
declare(strict_types=1);

namespace App\Presentation;

use App\Application\CreateWeatherReport;
use App\Application\CreateWeatherReportService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateWeatherReportCommand extends Command
{
    protected static $defaultName = 'app:create-weather-report';

    public function __construct(private CreateWeatherReportService $useCase)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('longitude', InputArgument::REQUIRED, 'Location longitude')
            ->addArgument('latitude', InputArgument::REQUIRED, 'Location latitude')
            ->addArgument('file-format', InputArgument::REQUIRED, 'Weather report file format');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $command = new CreateWeatherReport(
            longitude: $input->getArgument('longitude'),
            latitude: $input->getArgument('latitude'),
            fileFormat: $input->getArgument('file-format')
        );
        $this->useCase->execute($command);
        $output->writeln('Weather report was successfully created!');
        return Command::SUCCESS;
    }
}
