<?php
declare(strict_types=1);

namespace App\Application;


use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
final class GetWeatherReportResponse extends DataTransferObject
{
    public int $weatherReportTimestamp;
    public float $weatherReportTemperatureDegrees;
    public string $weatherReportTemperatureUnits;
    public int $weatherReportWindDirectionAmount;
    public string $weatherReportWindDirectionUnits;
    public float $weatherReportWindSpeedDistance;
    public string $weatherReportWindSpeedUnits;
    public int $weatherReportHumidityValue;
    public int $weatherReportVisibilityDistance;
    public string $weatherReportVisibilityUnits;
}