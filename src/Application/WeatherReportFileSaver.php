<?php
declare(strict_types=1);

namespace App\Application;

interface WeatherReportFileSaver
{
    public function save(string $fileName, string $contents): void;
}
