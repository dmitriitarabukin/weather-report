<?php
declare(strict_types=1);

namespace App\Application;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
final class CreateWeatherReport extends DataTransferObject
{
    public float $longitude;
    public float $latitude;
    public string $fileFormat;
}
