<?php
declare(strict_types=1);

namespace App\Application;

use App\Domain\Coordinates;
use App\Domain\Humidity;
use App\Domain\Temperature;
use App\Domain\Visibility;
use App\Domain\WeatherReport;
use App\Domain\WeatherReportDate;
use App\Domain\WeatherReportEncoderFactory;
use App\Domain\WeatherReportFileFormat;
use App\Domain\WindDirection;
use App\Domain\WindSpeed;

final class CreateWeatherReportService
{
    public function __construct(
        private WeatherReportProvider       $weatherReportProvider,
        private WeatherReportEncoderFactory $weatherReportEncoderFactory,
        private WeatherReportFileSaver      $weatherReportFileSaver
    )
    {
    }

    public function execute(CreateWeatherReport $command): void
    {
        $coordinates = Coordinates::fromFloat($command->longitude, $command->latitude);
        $fileFormat = WeatherReportFileFormat::fromString($command->fileFormat);

        $request = new GetWeatherReportRequest(longitude: $coordinates->longitude(), latitude: $coordinates->latitude());
        $response = $this->weatherReportProvider->weatherReport($request);
        $weatherReport = $this->weatherReportFromResponse($response, $coordinates);
        $encodedWeatherReport = $this->weatherReportEncoderFactory
            ->create()
            ->encodedWeatherReport($weatherReport, $fileFormat);
        $this->weatherReportFileSaver->save(
            $this->fileName($weatherReport->date(), $fileFormat),
            $encodedWeatherReport
        );
    }

    private function weatherReportFromResponse(GetWeatherReportResponse $response, Coordinates $coordinates): WeatherReport
    {
        return new WeatherReport(
            new WeatherReportDate($response->weatherReportTimestamp),
            new Temperature($response->weatherReportTemperatureDegrees, $response->weatherReportTemperatureUnits),
            new WindDirection($response->weatherReportWindDirectionAmount, $response->weatherReportWindDirectionUnits),
            new WindSpeed($response->weatherReportWindSpeedDistance, $response->weatherReportWindSpeedUnits),
            new Humidity($response->weatherReportHumidityValue),
            new Visibility($response->weatherReportVisibilityDistance, $response->weatherReportVisibilityUnits),
            $coordinates
        );
    }

    private function fileName(WeatherReportDate $date, WeatherReportFileFormat $fileFormat): string
    {
        return "weather_report_{$date->asAtomString()}.{$fileFormat->asString()}";
    }
}
