<?php
declare(strict_types=1);

namespace App\Application;

use App\Domain\WeatherReport;

abstract class WeatherReportView
{
    protected string $date;
    protected string $temperature;
    protected string $windDirection;
    protected string $windSpeed;
    protected string $humidity;
    protected string $visibility;
    protected string $longitude;
    protected string $latitude;

    private function __construct(WeatherReport $weatherReport)
    {
        $this->date = $weatherReport->date()->asAtomString();
        $this->temperature = $weatherReport->temperature()->asString();
        $this->windDirection = $weatherReport->windDirection()->asString();
        $this->windSpeed = $weatherReport->windSpeed()->asString();
        $this->humidity = $weatherReport->humidity()->asString();
        $this->visibility = $weatherReport->visibility()->asString();
        $this->longitude = (string)$weatherReport->coordinates()->longitude();
        $this->latitude = (string)$weatherReport->coordinates()->latitude();
    }

    public static function fromWeatherReport(WeatherReport $weatherReport): static
    {
        return new static($weatherReport);
    }

    abstract public function asArray(): array;
}
