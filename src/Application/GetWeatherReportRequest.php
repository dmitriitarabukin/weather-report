<?php
declare(strict_types=1);

namespace App\Application;


use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
final class GetWeatherReportRequest extends DataTransferObject
{
    public string $longitude;
    public string $latitude;
}