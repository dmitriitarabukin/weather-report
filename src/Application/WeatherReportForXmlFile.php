<?php
declare(strict_types=1);

namespace App\Application;


final class WeatherReportForXmlFile extends WeatherReportView
{
    public function asArray(): array
    {
        return [
            'Дата' => $this->date,
            'Скорость_ветра' => $this->windSpeed,
            'Температура' => $this->temperature,
            'Направление_ветра' => $this->windDirection,
            'Влажность' => $this->humidity,
            'Видимость' => $this->visibility,
            'Координаты' => [
                'Долгота' => $this->longitude,
                'Широта' => $this->latitude,
            ]
        ];
    }
}
