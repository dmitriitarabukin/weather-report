<?php
declare(strict_types=1);

namespace App\Application;


final class WeatherReportForJsonFile extends WeatherReportView
{
    public function asArray(): array
    {
        return [
            'Дата' => $this->date,
            'Температура' => $this->temperature,
            'Направление ветра' => $this->windDirection,
            'Скорость ветра' => $this->windSpeed,
            'Влажность' => $this->humidity,
            'Видимость' => $this->visibility,
            'Координаты' => [
                'Долгота' => $this->longitude,
                'Широта' => $this->latitude,
            ]
        ];
    }
}
