<?php
declare(strict_types=1);

namespace App\Application;

interface WeatherReportProvider
{
    public function weatherReport(GetWeatherReportRequest $getWeatherReportRequest): GetWeatherReportResponse;
}
