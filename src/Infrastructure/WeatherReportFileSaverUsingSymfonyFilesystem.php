<?php
declare(strict_types=1);

namespace App\Infrastructure;


use App\Application\WeatherReportFileSaver;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;

final class WeatherReportFileSaverUsingSymfonyFilesystem implements WeatherReportFileSaver
{
    public function __construct(private Filesystem $filesystem, private string $storageDir)
    {
    }

    public function save(string $fileName, string $contents): void
    {
        $this->filesystem->dumpFile(
            Path::makeAbsolute($fileName, $this->storageDir),
            $contents
        );
    }
}
