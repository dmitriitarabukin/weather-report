<?php
declare(strict_types=1);

namespace App\Infrastructure;

use App\Application\GetWeatherReportRequest;
use App\Application\GetWeatherReportResponse;
use App\Application\WeatherReportProvider;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

final class OpenWeatherMapWeatherReportProvider implements WeatherReportProvider
{
    public function __construct(
        private ClientInterface  $client,
        private DecoderInterface $jsonDecoder,
        private string           $baseApiResource,
        private string           $apiKey
    )
    {
    }

    public function weatherReport(GetWeatherReportRequest $getWeatherReportRequest): GetWeatherReportResponse
    {
        $request = new Request('GET', $this->url($getWeatherReportRequest));
        $response = $this->client->sendRequest($request);
        return $this->weatherReportResponseDTOFromResponse($response);
    }

    private function url(GetWeatherReportRequest $request): string
    {
        return $this->baseApiResource . '?' . http_build_query([
                'lon' => $request->longitude,
                'lat' => $request->latitude,
                'appid' => $this->apiKey,
                'units' => 'metric',
            ]);
    }

    private function weatherReportResponseDTOFromResponse(ResponseInterface $response): GetWeatherReportResponse
    {
        $decodedResponse = $this->jsonDecoder->decode(
            $response->getBody()->getContents(),
            JsonEncoder::FORMAT,
            [JsonDecode::ASSOCIATIVE]
        );

        return new GetWeatherReportResponse(
            weatherReportTimestamp: $decodedResponse['dt'],
            weatherReportTemperatureDegrees: $decodedResponse['main']['temp'],
            weatherReportTemperatureUnits: 'C',
            weatherReportWindDirectionAmount: $decodedResponse['wind']['deg'],
            weatherReportWindDirectionUnits: 'deg.',
            weatherReportWindSpeedDistance: $decodedResponse['wind']['speed'],
            weatherReportWindSpeedUnits: 'm/s',
            weatherReportHumidityValue: $decodedResponse['main']['humidity'],
            weatherReportVisibilityDistance: $decodedResponse['visibility'],
            weatherReportVisibilityUnits: 'm'
        );
    }
}
