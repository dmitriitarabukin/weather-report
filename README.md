### Задача
Необходимо реализовать функционал, который будет получать информацию о
погоде из любого публичного API. 
Он должен уметь сохранять полученные данные в файле формата json и xml в
зависимости от переданного в него параметра типа файла.
Задание необходимо выполнить с использованием ООП, постараться применить паттерны проектирования.

Для JSON первыми по порядку должны быть поля:
Дата
Температура
Направление ветра
т.д.

Для XML:
Дата
Скорость ветра
Температура
т.д.

### Installation
1. Install Docker 
2. Install Make utility (if on Windows - use Cygwin or Chocolatey)
3. Run 'make start' from the project root

### Usage
make sf c='app:create-weather-report {longitude} {latitude} {file-format}'

### Example
make sf c='app:create-weather-report 30.308611 59.9375 json'

### Examine weather reports
Weather reports are located in /storage
